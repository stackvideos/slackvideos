const { Pool } = require('pg');
const helper = require("../helper/helper");
const configInfo = helper.yamlToJson();

const pool = new Pool({
    host: configInfo.database.host,
    user: configInfo.database.user,
    password: configInfo.database.password,
    database: configInfo.database.db,
    max: configInfo.database.max,
    idleTimeoutMillis: configInfo.database.idleTimeoutMillis,
    connectionTimeoutMillis: configInfo.database.connectionTimeoutMillis,
});

try {
    pool.connect(function(err) {
        if (err) throw err;
       console.log("Connected Successfully ...");
    })
} catch (e) {
    console.error("Failed to connect ...", e);
}


module.exports = pool;


const express = require("express");
const isLoggedIn = require("../../middleware/isLoggedIn");
const AnswerController = require("../../controller/answers/AnswerController");
const multer  = require('multer')
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        console.log("file name is", file);
      cb(null, '../upload/')
    },
    onError : function(err, next) {
        console.log('error', err);
        next(err);
    },
    filename: function (req, file, cb) {
      const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
      cb(null, file.fieldname + '-' + uniqueSuffix)
    }
})
  
const upload = multer({ storage: storage })
const router = express.Router();

router.post("/", upload.single("videoAnswer"), function (req, res, next) {
    AnswerController.postAnswer(req, res);
});

router.get("/:ques_id", function (req, res) {
    AnswerController.fetchAnswer(req, res);
});


router.post("/accept/:answer_id", function (req, res) {
    AnswerController.acceptAnswer(req, res);
});

router.post("/reject/:answer_id", function (req, res) {
    AnswerController.rejectAnswer(req, res);
});



module.exports = router;
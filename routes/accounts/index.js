const express = require("express");
const AccountControler = require("../../controller/accounts/AccountControler");
const router = express.Router();

router.post("/register", function (req, res) {
    AccountControler.registerUser(req, res);
    
});

router.post("/login", function (req, res) {
    AccountControler.loginUser(req, res);
});


router.get("/", function (req, res) {
    AccountControler.getUser(req, res);
});

router.post("/update", function (req, res) {
    AccountControler.updateUser(req, res);
});

router.post("/upload", function (req, res) {
    AccountControler.uploadProfileImage(req, res);
});


module.exports = router;
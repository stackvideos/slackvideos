var express = require('express');
var router = express.Router();
const manifestFile = require("../public/build/rev-manifest.json");
const isLoggedIn = require("../middleware/isLoggedIn");
const QuestionController = require('../controller/questions/QuestionController');
/* GET home page. */
router.get('/login', [isLoggedIn], function (req, res, next) {
    res.render('auth/login', {title: 'Express', layout: "nonAuth", stylePath: manifestFile["stylesheet.css"], scriptPath: manifestFile["bundle.js"]});
});


router.get("/", async function (req, res, next) {
    const isLoggedIn = (req.session.userId) ? true : false;
    await QuestionController.fetchAllQuestion().then((data) => {
        res.render("pages/home", {title: 'Express', layout: "nonAuth", stylePath: manifestFile["stylesheet.css"], scriptPath: manifestFile["bundle.js"], isLoggedIn, questions: data});
    }).catch((err) => {
        res.render("pages/home", {title: 'Express', layout: "nonAuth", stylePath: manifestFile["stylesheet.css"], scriptPath: manifestFile["bundle.js"], isLoggedIn, questions: []});
    });
    
});

router.get("/questions/:id", async function (req, res, next) {
    const isLoggedIn = (req.session.userId) ? true : false;
    res.render("pages/questions", {title: 'Questions', layout: "nonAuth", stylePath: manifestFile["stylesheet.css"], scriptPath: manifestFile["bundle.js"], isLoggedIn});
   
   
});


router.get("/profile", async function (req, res, next) {
    const isLoggedIn = (req.session.userId) ? true : false;
    res.render("pages/profile", {title: 'Express', layout: "nonAuth", stylePath: manifestFile["stylesheet.css"], scriptPath: manifestFile["bundle.js"], isLoggedIn});
});


router.get("/question/add",  function (req, res, next) {
    const isLoggedIn = (req.session.userId) ? true : false;
    res.render("pages/questionPost", {title: 'Questions', layout: "nonAuth", stylePath: manifestFile["stylesheet.css"], scriptPath: manifestFile["bundle.js"], isLoggedIn });
});


router.get("/logout", function (req, res) {
    req.session.destroy();
    res.redirect("/");
})

module.exports = router;

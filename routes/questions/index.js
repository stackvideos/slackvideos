const express = require("express");
const isLoggedIn = require("../../middleware/isLoggedIn");
const QuestionController = require("../../controller/questions/QuestionController");
const router = express.Router();

router.post("/", function (req, res) {
    QuestionController.postQuestion(req, res);
});


router.get("/:id", function (req, res) {
    QuestionController.fetchQuestion(req, res);
});

router.get("/", function (req, res) {
    QuestionController.fetchAllQuestion(req, res);
});

router.post("/follow/:id", function (req, res) {
    QuestionController.followQuestion(req, res);
});



router.post("/unfollow/:id", function (req, res) {
    QuestionController.unFollowQuestion(req, res);
});


router.get("/follow/:id", function (req, res) {
    QuestionController.getFollowingUser(req, res);
});

module.exports = router;
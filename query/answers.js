module.exports = {
    postAnswers: function (answer_id, user_id, answer_description, question_id) {
        return `INSERT INTO answers(answer_id, user_id, question_id, answer_description , answered_on)VALUES('${answer_id}', '${user_id}', '${question_id}', '${answer_description}', NOW());`
    },
    getAnswers: function (questionId) {
        return `SELECT * from answers INNER JOIN accounts ON answers.user_id = accounts.user_id where answers.question_id='${questionId}'  ORDER BY answers.answered_on;`
    },

    acceptAnswer: function (answer_id) {
        return `UPDATE answers SET follow_count = follow_count + 1, unfollow_count = unfollow_count - 1 WHERE answer_id = '${answer_id}' RETURNING *;`
    },

    rejectAnswer: function (answer_id) {
        return `UPDATE answers SET follow_count = follow_count - 1, unfollow_count = unfollow_count + 1 WHERE answer_id = '${answer_id}' RETURNING *;`
    },

    insertAcceptUser: function (data, answer_id) {
        return `UPDATE answers SET actionuser = '${data}' WHERE answer_id = '${answer_id}';`
    }
}
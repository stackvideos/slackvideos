module.exports = {
    createUserTable: function() {
        return `CREATE TABLE IF NOT EXISTS accounts (
            user_id VARCHAR(255) PRIMARY KEY,
            username VARCHAR (255) UNIQUE NOT NULL,
            password VARCHAR (255) NOT NULL,
            first_name VARCHAR(255),
            last_name VARCHAR (255),
            email VARCHAR (255) UNIQUE NOT NULL,
            mobile VARCHAR(255),
            total_points INT DEFAULT 1,
            profile_image VARCHAR(255),
            is_email_verified boolean DEFAULT False,
            is_mobile_verified boolean DEFAULT False,
            is_verified boolean,
            verification_code smallint,
            register_date TIMESTAMP NOT NULL,
            verified_on TIMESTAMP,
            last_updated TIMESTAMP,
            last_login TIMESTAMP 
        );`
    },

    createQuestonsTable: function() {
        return `CREATE TABLE IF NOT EXISTS questions (
            question_id VARCHAR(255) PRIMARY KEY,
            user_id VARCHAR (255) NOT NULL,
            questions_title VARCHAR (255) NOT NULL,
            questions_description VARCHAR NOT NULL,
            math_data VARCHAR,
            questions_tag VARCHAR(255) NOT NULL,
            accepted boolean DEFAULT False,
            answer_count int DEFAULT 0,
            video_count int DEFAULT 0,
            follow_count int DEFAULT 0,
            unfollow_count int DEFAULT 0,
            asked_on TIMESTAMP NOT NULL
        );`
    },

    answerQuestionTable: function() {
        return `CREATE TABLE IF NOT EXISTS answers (
            answer_id VARCHAR(255) PRIMARY KEY,
            user_id VARCHAR (255) NOT NULL,
            question_id VARCHAR (255) NOT NULL,
            answer_description VARCHAR NOT NULL,
            math_data VARCHAR,
            accepted boolean DEFAULT False,
            video_url VARCHAR (255),
            video_count int DEFAULT 0,
            follow_count int DEFAULT 0,
            unfollow_count int DEFAULT 0,
            actionUser json,
            answered_on TIMESTAMP NOT NULL
        );`
    },

    followQuestions: function() {
        return `CREATE TABLE IF NOT EXISTS followquesstions (
            follow_id VARCHAR(255) PRIMARY KEY,
            question_id VARCHAR(255),
            user_id VARCHAR(255),
            followed_on TIMESTAMP NOT NULL
            
        );`
    },

    acceptAnswer: function() {
        return `CREATE TABLE IF NOT EXISTS acceptanswer (
            accept_id VARCHAR(255) PRIMARY KEY,
            answer_id VARCHAR(255),
            user_id VARCHAR(255),
            isaccept BOOLEAN,
            isreject BOOLEAN,
            author_accepted BOOLEAN,
            accepted_on TIMESTAMP NOT NULL
            
        );`
    },
}
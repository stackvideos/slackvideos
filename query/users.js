module.exports = {
    registerUser: function (user_id, username, email, password, verification_code) {
        return `INSERT INTO accounts(user_id, username, password, email, is_verified, verification_code, register_date)VALUES('${user_id}', '${username}', '${password}', '${email}', False, '${verification_code}', NOW());`
    },

    loginUser: function (username) {
        return `SELECT user_id, username, password, email, register_date, last_login from accounts where username='${username}'`
    },

    getProfile: function (userId) {
        return `SELECT * from accounts where user_id='${userId}'`
    },

    updateProfile: function (user) {
        return `UPDATE accounts SET first_name = '${user.first_name}', last_name = '${user.last_name}' WHERE user_id = '${user.user}';`
    },

    uploadImage: function (user) {
        return `UPDATE accounts SET profile_image = '${user.profile}' WHERE user_id = '${user.user}';`
    }
}
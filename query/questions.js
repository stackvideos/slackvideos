module.exports = {
    postQuestions: function (question_id, user_id, questions_title, questions_description, questions_tag, math_data) {
        return `INSERT INTO questions(question_id, user_id, questions_title , questions_description, questions_tag, math_data, asked_on)VALUES('${question_id}', '${user_id}', '${questions_title}', '${questions_description}','${questions_tag}', '${math_data}', NOW()) RETURNING *;`
    },

    fetchQuestion: function (id) {
        return `SELECT * from questions where question_id='${id}'`
    },

    fetchAllQuestions: function() {
        return `SELECT * from questions`
    },

    followQuestion: function (ques_id) {
        return `UPDATE questions SET follow_count = follow_count + 1 WHERE question_id = '${ques_id}';`
    },

    unFollowQuestion: function (ques_id) {
        return `UPDATE questions SET follow_count = follow_count - 1 WHERE question_id = '${ques_id}';`
    },

    updateAnswerCount: function (ques_id) {
        return `UPDATE questions SET answer_count = answer_count + 1 WHERE question_id = '${ques_id}';`
    }
    



}
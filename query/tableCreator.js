const tableCreator = require("./table");
const pool = require("../database/db");

pool.query(tableCreator.createUserTable(), function (req, res) {
    console.log("Table Created successfully");
});

pool.query(tableCreator.createQuestonsTable(), function (req, res) {
    console.log("Table Created successfully");
});

pool.query(tableCreator.answerQuestionTable(), function (req, res) {
    console.log("Table Created successfully");
});

pool.query(tableCreator.followQuestions(), function (req, res) {
    console.log("Table Created successfully");
});

pool.query(tableCreator.acceptAnswer(), function (req, res) {
    console.log("Table Created successfully");
});

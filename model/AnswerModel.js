const AnswerQuery = require("../query/answers");
const QuestionQuery = require("../query/questions");
const pool = require("../database/db");
const uuid = require("uuid");
const acceptanswer = require("../query/acceptanswer");

module.exports = {
    postAnswer: async function (answer) {
        const {description, questionId, user} = answer;
        const answerId = uuid.v4();
        return await new Promise(async (resolve, reject) => {
            await pool.query(AnswerQuery.postAnswers(answerId, user, description, questionId) , async (err, body) => {
                
                if(err) {
                    reject({status: 500});
                }

                await pool.query(QuestionQuery.updateAnswerCount(questionId), (err, body) => {
                    resolve({status:200, message: "Answered Successfully"});
                });
            });
        });

    },


    fetchAnswer: async function(questionId) {
        return await new Promise(async (resolve, reject) => {
            await pool.query(AnswerQuery.getAnswers(questionId) , (err, body) => {
                if(err) {
                    reject({status: 500});
                }

                
                resolve(body.rows);
            });
        });
    },

    acceptAnswer: async function (answer_id, user) {
        return await new Promise(async (resolve, reject) => {
            await pool.query(AnswerQuery.acceptAnswer(answer_id) , async (err, body) => {
                
                if(err) {
                    reject({status: 500});
                }
                let data = body.rows[0].actionuser || {};
                data[user] = "true";
                
                pool.query(AnswerQuery.insertAcceptUser(JSON.stringify(data), answer_id), (err, body) => {
                    resolve({message: "Solution Accepted"});
                });
                // pool.query(acceptanswer.acceptAnswer(accept_id, answer_id, user) , (err, body) => {
                //     resolve({message: "Solution Accepted"});
                // })
                
            });
        });
    },


    rejectAnswer: async function (answer_id, user) {
        return await new Promise(async (resolve, reject) => {
            await pool.query(AnswerQuery.rejectAnswer(answer_id) , async (err, body) => {
                
                if(err) {
                    reject({status: 500});
                }
                let data = body.rows[0].actionuser || {};
                data[user] = "false";
                pool.query(AnswerQuery.insertAcceptUser(JSON.stringify(data), answer_id), (err, body) => {
                    resolve({message: "Solution Accepted"});
                });
                
                // pool.query(acceptanswer.acceptAnswer(accept_id, answer_id, user) , (err, body) => {
                //     resolve({message: "Solution Accepted"});
                // })
                
            });
        });
    }

    
}
const UserQuery = require("../query/users");
const pool = require("../database/db");
const uuid = require("uuid");
module.exports = {
    registerUser: async function (user) {
        const {username, email, password, verificationCode} = user;
        const userId = uuid.v4();
       
        await new Promise((resolve, reject) => {
            pool.query(UserQuery.registerUser(userId, username, email, password, verificationCode), function (err, body) {
                console.log("Error occured", err);
                if (err) {
                    reject({status: 500, constraint: err.constraint});
                } 
                resolve({});
            })
         
        })
        
    },

    loginUser: async function (user) {
        const {username} = user;
        return await new Promise(async (resolve, reject) => {
            await pool.query(UserQuery.loginUser(username), async function (err, body) {
                if (err) {
                    reject({status: 500, constraint: err.constraint});
                }
                await resolve(body.rows[0]);
            })
        });
    },


    getProfile: async function (userId) {
        return await new Promise(async (resolve, reject) => {
            await pool.query(UserQuery.getProfile(userId), async function (err, body) {
                
                if (err) {
                    reject({status: 500, constraint: err.constraint});
                }
                await resolve(body.rows[0]);
            })
        });
    },

    updateProfile: async function(user) {
        return await new Promise(async (resolve, reject) => {
            await pool.query(UserQuery.updateProfile(user), async function (err, body) {
                console.log("err received", err);
                if (err) {
                    reject({status: 500, constraint: err.constraint});
                }
                await resolve({status: "profile updated successfully"});
            })
        });
    },

    uploadImage: async function (image) {
        return await new Promise(async (resolve, reject) => {
            await pool.query(UserQuery.uploadImage(image), async function (err, body) {
                console.log("err received", err);
                if (err) {
                    reject({status: 500, constraint: err.constraint});
                }
                await resolve({status: "profile updated successfully"});
            })
        });
    }
}
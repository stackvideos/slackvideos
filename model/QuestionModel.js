const QuestionQuery = require("../query/questions");
const pool = require("../database/db");
const uuid = require("uuid");
const { followQuestion, isFollowing, unFollowQuestion } = require("../query/followQuestion");

module.exports = {
    postQuestion: async function (questions) {
        const {user, title, description, tags, mathData} = questions;
        const questionId = uuid.v4();
        return await new Promise(async (resolve, reject) => {
            await pool.query(QuestionQuery.postQuestions(questionId, user, title, description, tags, mathData) , (err, body) => {
                
                if(err) {
                    reject({status: 500});
                }
                resolve({status:200, message: "Question Created Successfully", questionId: body.rows[0].question_id});
            })
        });

    },

    fetchQuestion: async function (id) {
        return await new Promise(async (resolve, reject) => {
            await pool.query(QuestionQuery.fetchQuestion(id) , (err, body) => {

                console.log("Body Received", body);
                
                if(err) {
                    reject({status: 500});
                }
                resolve(body);
            })
        });
    },

    fetchAllQuestion: async function () {
        return await new Promise(async (resolve, reject) => {
            await pool.query(QuestionQuery.fetchAllQuestions() , (err, body) => {
                if(err) {
                    reject({status: 500});
                }
                resolve(body);
            })
        });
    },

    followQuestion: async function (id, user) {
        return await new Promise(async (resolve, reject) => {
            await pool.query(QuestionQuery.followQuestion(id) , (err, body) => {
                const follow_id = uuid.v4();
                pool.query(followQuestion(follow_id, id, user) , (err, body) => {
                    resolve({status: true, message: "Followed Successfully"});
                }); 
                if(err) {
                    reject({status: 500});
                }
            });
        });
    },

    unFollowQuestion: async function (id, user) {
        return await new Promise(async (resolve, reject) => {
            await pool.query(QuestionQuery.unFollowQuestion(id) , (err, body) => {
                
                pool.query(unFollowQuestion(id, user) , (err, body) => {
                    resolve({status: true, message: "unFollowed Successfully"});
                }); 
                if(err) {
                    reject({status: 500});
                }
            });
        });
    },


    getFollowingUser: async function (id, user) {
        return await new Promise(async (resolve, reject) => {
            
            pool.query(isFollowing(id, user) , (err, body) => {
                if(err) {
                    reject({status: 500});
                }
                
                    resolve(body.rows);
                }); 
                
            });
    
    }


}
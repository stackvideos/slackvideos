const yaml = require('js-yaml');
const fs = require("fs");
const { dirname } = require('path');
const appDir = dirname(__filename);

module.exports = {
    yamlToJson: function() {
        const inputFile = `${appDir}/default.yml`;
        let obj = yaml.load(fs.readFileSync(inputFile, {encoding: 'utf-8'}));
        return obj;
    }
}
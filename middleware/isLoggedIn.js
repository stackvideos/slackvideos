function isLoggedIn(req, res, next) {
    console.log(req.session);
    if (req.session.userId) {
        res.redirect("/");
    } else {
        next();
    }
    
}

module.exports = isLoggedIn;
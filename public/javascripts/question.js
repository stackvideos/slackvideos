app.controller('questionPostController', async function ($scope, $http) {
   $scope.tags = "React";
    $scope.postQuestion = async function () {
        let questionBody = {
            title: $scope.title,
            description: $scope.description,
            tags: $scope.tags
        }

        // const formulaBody = document.querySelector('#formula').innerHTML;
        // const extractQuote = formulaBody
        //     .match(/(?:"[^"]*"|^[^"]*$)/)[0]
        //     .replace(/"/g, "")

        // questionBody["mathData"] = extractQuote;

        $http.post(`/api/questions`, JSON.stringify(questionBody), { 'Content-Type': 'application/json' })
            .then((response) => {
                window.location.href = `/questions/${response.data.questionId}`;
            }).catch((err) => {

        });
    }


});




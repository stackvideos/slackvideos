const toggleForm = () => {
    const container = document.querySelector('.loginForm');
    container.classList.toggle('active');
};

//--------------- USER REGISTRATION ----------------------------- //

document.querySelector('#registerForm').addEventListener('submit', async (e) => {
  e.preventDefault();
  const formData = new FormData(e.target);

  let registerBody = {
    username: formData.get("username"),
    password: formData.get("password"),
    confirmPassword: formData.get("confirmPassword"),
    email: formData.get("email")
  }

  const response = await fetch('/account/register', {
    method: 'POST',
    body: JSON.stringify(registerBody), // string or object
    headers: {
      'Content-Type': 'application/json'
    }
  });

  const myJson = await response.json();
  if (myJson.status === 200) {
    document.getElementById("success-message").style.display = "block";
    document.getElementById("success-message").innerHTML = myJson.message;
    setTimeout(() => {
      document.getElementById("success-message").style.display = "none";
    }, 1000);
  } else {
    document.getElementById("error-message").style.display = "block";
    document.getElementById("error-message").innerHTML = myJson;
    setTimeout(() => {
      document.getElementById("error-message").style.display = "none";
    }, 1000);
  } 
});


//----------------- LOGIN FORM --------------------------- //


document.querySelector('#loginForm').addEventListener('submit', async (e) => {
  e.preventDefault();
  const formData = new FormData(e.target);

  let loginBody = {
    username: formData.get("username"),
    password: formData.get("password"),
  }

  const response = await fetch('/account/login', {
    method: 'POST',
    body: JSON.stringify(loginBody), // string or object
    headers: {
      'Content-Type': 'application/json'
    }
  });

  const myJson = await response.json();
  if (myJson.status === 200) {
      window.location.reload();
  } else {
      console.error("Some error occured");
  } 
});
app.directive('customOnChange', function() {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        var onChangeHandler = scope.$eval(attrs.customOnChange);
        element.on('change', onChangeHandler);
        element.on('$destroy', function() {
          element.off();
        });
  
      }
    };
});


app.controller('profileController', async function ($scope, $http) {
    $scope.user = {};
    $scope.profile_image = "";

    $scope.getUser = function () {
        $http.get("/account").then((response) => {
            $scope.user = response.data;
    
            if (!first_name in $scope.user) {
                $scope.user.first_name = "";
            }
    
            if (!last_name in $scope.user) {
                $scope.user.last_name = "";
            }
        }).catch((err) => {
    
        });
    }
    


    $scope.updateResult = function () {
        let userBody = $scope.user

        // const formulaBody = document.querySelector('#formula').innerHTML;
        // const extractQuote = formulaBody
        //     .match(/(?:"[^"]*"|^[^"]*$)/)[0]
        //     .replace(/"/g, "")

        // questionBody["mathData"] = extractQuote;

        $http.post(`/account/update`, JSON.stringify(userBody), { 'Content-Type': 'application/json' })
            .then((response) => {
                $scope.getUser();
            }).catch((err) => {

        });
    }

    $scope.selectImage = function (event) {
        $scope.profile_image = event.target.files[0];
        console.log($scope.profile_image)
    }

    $scope.onUploadSelect = function () {
        const formData = new FormData();
        formData.append("image", $scope.profile_image);
        $http.post('/account/upload', formData, {headers: { 'Content-Type': undefined}})
        .then(function(data) {
            $scope.getUser();
        });
    }
    
    $scope.getUser();
});
 
 
 
 
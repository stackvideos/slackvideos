const segments = new URL(window.location.href).pathname.split('/');
const last = segments.pop() || segments.pop(); // Handle potential trailing slash


app.directive('fileModel', ['$parse', function($parse){
	return {
		restrict: 'A',
		link: function(scope, element, attrs){
			var model = $parse(attrs.fileModel);
			var modelSetter = model.assign;

			element.bind('change', function(){
				scope.$apply(function(){
					modelSetter(scope, element[0].files[0]);
				})
			})
		}
	}
}])



app.controller('answerController', function($scope, $http, $sce) {
    $scope.isFollowing = false;
    $scope.getAnswer = function () {
        $http.get(`/api/answers/${last}`).then((response) => {
            let answers = response.data;
    
            answers.forEach(each_answer => {
                each_answer.answer_description = $sce.trustAsHtml(each_answer.answer_description);
            });
            $scope.answers  = answers;
        })
    }


    $scope.submitAnswer = async function () {
        let answerBody = {
            description: document.getElementsByClassName("ql-editor")[0].innerHTML.replace(/'/g, '"'),
            questionId: last
        }
        
        const response = await fetch('/api/answers/', {
            method: 'POST',
            body: JSON.stringify(answerBody), // string or object
            headers: {
              'Content-Type': 'application/json'
            }
        });
        $scope.loadQuestion();
        $scope.getAnswer();
    }

    $scope.loadQuestion = async function () {
        $http.get(`/api/questions/${last}`).then((response) => {
            $scope.question = response.data;
        })
    }

    $scope.followQuestion = async function () {
        await $http.post(`/api/questions/follow/${last}`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json'
            }
        }).then((response) => {
            $scope.loadQuestion();
            $scope.ifUserFollowQuestion();
        }).catch((err) => {

        });
    }


    $scope.ifUserFollowQuestion = function () {
        $http.get(`/api/questions/follow/${last}`).then((response) => {
           if (response.data.length > 0) {
               $scope.isFollowing = true;
           } else {
               $scope.isFollowing = false;
           }
        })
    }

    $scope.unFollowQuestion = async function () {
        await $http.post(`/api/questions/unfollow/${last}`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json'
            }
        }).then((response) => {
            $scope.loadQuestion();
            $scope.ifUserFollowQuestion();
        }).catch((err) => {

        });
    }

    $scope.markCorrect = function (answer_id) {
        $http.post(`/api/answers/accept/${answer_id}`).then((response) => {
            $scope.getAnswer();
        }).catch((err) => {

        });
    }


    $scope.markIncorrect = function (answer_id) {
        $http.post(`/api/answers/reject/${answer_id}`).then((response) => {
            $scope.getAnswer();
        }).catch((err) => {

        });
    }


    $scope.openCodeEditor = function () {
        alert("To Open Code Editor");
    }


    $scope.uploadVideo = function () {
        var fd = new FormData();
		for(var key in $scope.supportingVideo)
			fd.append(key, $scope.supportingVideo[key]);
		$http.post(`/api/answers/`, {videoAnswer: fd}, {
			transformRequest: angular.indentity,
			headers: { 'Content-Type': undefined }
		});
    }

    $scope.loadQuestion();
    $scope.getAnswer();
    $scope.ifUserFollowQuestion();


   
});


document.getElementById("videoUpload")
.onchange = function(event) {
  let file = event.target.files[0];
  let blobURL = URL.createObjectURL(file);
  document.querySelector("video").src = blobURL;
}
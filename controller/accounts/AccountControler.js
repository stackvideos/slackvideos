const AccountModel = require("../../model/AccountModel");
const messageJson = require("../../message/message.json");
const constraints = require("../../message/constraint.json");
const bcrypt = require("bcrypt");
const saltRounds = 10;
const multer  = require('multer')

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/uploads/')
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + '.jpg') //Appending .jpg
    }
  })
  
var upload = multer({ storage: storage });
module.exports = {
    registerUser: async function (req, res) {
        let { username, password, confirmPassword, email } = req.body;
       
        if (password !== confirmPassword) {
            return res.status(500).json(messageJson.passwordNotMatch);
        }
        
        await bcrypt.hash(password, saltRounds, async function(err, hash) {
            password = hash;
            const verificationCode =  Math.floor(1000 + Math.random() * 9000);
            return await AccountModel.registerUser({username, email, password, verificationCode}).then((d) => {
                res.status(200).json({status: 200, message: messageJson.registerSuccess});
            }).catch((err) => {
                let message = messageJson.registerUnknownError;
                switch (err.constraint) {
                    case constraints.username:
                        message = messageJson.userNameAlreadyExist;
                        break;
                    case constraints.email:
                        message = messageJson.emailAlreadyExist;
                        break;
                }
                res.status(err.status).json(message);
            });
        });
    },

    loginUser: async function (req, res) {
        const {username, password} = req.body;
        
            await AccountModel.loginUser({username}).then(d => {
                if (d) {
                    bcrypt.compare(password, d.password, function (err, result) {
                        if (result) {
                            req.session.userId = d.user_id;
                            return res.status(200).json({status: 200, id: d.user_id});
                        } else {
                            return res.status(500).json({message: "Invaid username or password"});
                        }
                    })
                }
            }).catch((err) => {
                return res.status(500).json({message: "Some error occured"});
            })
        
        
    },


    getUser: async function (req, res) {

            const user = req.session.userId || req.query.user;
            
            await AccountModel.getProfile(user).then(d => {
                console.log("d received", d);
                res.status(200).json(d);
            }).catch((err) => {
                return res.status(500).json({message: "Some error occured"});
            })
        
        
    },

    updateUser: async function (req, res)  {
        const {first_name, last_name} = req.body;
        const user = req.session.userId;
        console.log("REQUEST BODY IS", req.body);
        await AccountModel.updateProfile({first_name, last_name, user}).then(d => {
            
            res.status(200).json(d);
        }).catch((err) => {
            return res.status(500).json({message: "Some error occured"});
        })

    },

    uploadProfileImage: function (req, res) {
        const uploaders = upload.single('image');
        const user = req.session.userId;
        uploaders(req, res, async function (err) {
            console.log("file is ", req.file.filename);
            const profile_image_path = `http://localhost:3000/uploads/${req.file.filename}`; 
            if (err instanceof multer.MulterError) {
                // A Multer error occurred when uploading.
            } else if (err) {
                // An unknown error occurred when uploading.
            }
            // Everything went fine. 

            await AccountModel.uploadImage({profile: profile_image_path, user}).then(d => {
            
                res.status(200).json(d);
            }).catch((err) => {
                return res.status(500).json({message: "Some error occured"});
            })

          
           
        })
    }
    
}


/*
check for unique id

check for unique username

check for unique email
*/


const QuestionModel = require("../../model/QuestionModel");


module.exports = {
    postQuestion: async function(req , res) {
        const user = req.session.userId;
        const {title, description, tags, mathData} = req.body;
        await QuestionModel.postQuestion({user, title, description, tags, mathData}).then((data) => {
            res.status(200).json({questionId: data.questionId});
        }).catch((err) => {
            res.status(err.status).json(err);
        });
    },

    fetchQuestion: async function (req, res) {
        const id = req.params.id;
        return await new Promise (async (resolve, reject) => {
            await QuestionModel.fetchQuestion(id).then((data) => {
                res.json(data.rows[0]);
            }).catch((err) => {
                reject(err);
            });
        })
        
    },


    fetchAllQuestion: async function (req, res) {
       
        return await new Promise (async (resolve, reject) => {
            await QuestionModel.fetchAllQuestion().then((data) => {
                resolve(data.rows);
            }).catch((err) => {
                reject(err);
            });
        })
        
    },


    followQuestion: async function (req, res) {
        const id = req.params.id;
        const user = req.session.userId;
        await QuestionModel.followQuestion(id, user).then((data) => {
            res.json(data);
        }).catch((err) => {
            
        }); 
    },

    getFollowingUser: async function (req, res) {
        const id = req.params.id;
        const user = req.session.userId;
        await QuestionModel.getFollowingUser(id, user).then((data) => {
            res.json(data);
        }).catch((err) => {
            
        }); 
    },

    unFollowQuestion: async function (req, res) {
        const id = req.params.id;
        const user = req.session.userId;
        await QuestionModel.unFollowQuestion(id, user).then((data) => {
            res.json(data);
        }).catch((err) => {
            
        }); 
    }
}
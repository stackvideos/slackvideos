const AnswerModel = require("../../model/AnswerModel");

module.exports = {
    postAnswer: async function (req,  res) {
        const user = req.session.userId;
        const {description, questionId} = req.body;
        
        await AnswerModel.postAnswer({description, questionId, user}).then((data) => {
           res.status(200).json({status: true, message: "Asnwered Succesfully"});
        }).catch((err) => {
            res.status(500).json(err);
        });
    },

    fetchAnswer: async function (req, res) {
        const user = req.session.userId;
        await AnswerModel.fetchAnswer(req.params.ques_id).then((data) => {
         
                data.forEach((d) => {
                if (d.actionuser !== null || d.actionuser) {
                    if (user in d.actionuser) {
                        d.status = d.actionuser[user] == "true" ? "accepted" : "rejected"
                     } else {
                         d.status = "No Response";
                     }
                     delete d.actionuser;
                }
                
            })
            res.status(200).json(data);
       
        }).catch((err) => {
            console.log("Error received", err);
            res.status(500).json(err);
        });
    },

    acceptAnswer: async function (req, res) {
        const user = req.session.userId;
        await AnswerModel.acceptAnswer(req.params.answer_id, user).then((data) => {
            res.status(200).json(data);
         }).catch((err) => {
             res.status(err.status).json(err);
         });
    },

    rejectAnswer: async function (req, res) {
        const user = req.session.userId;
        await AnswerModel.rejectAnswer(req.params.answer_id, user).then((data) => {
            res.status(200).json(data);
         }).catch((err) => {
             res.status(err.status).json(err);
         });
    }
}
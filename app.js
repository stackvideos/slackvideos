var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
const accounts = require("./routes/accounts/index.js");
const questions = require("./routes/questions/index.js");
const answer = require("./routes/answer/index.js");
//const db = require("./database/db");
const exphbs = require('express-handlebars');
const compression = require('compression');
const helmet = require("helmet");
var csrf = require('csurf');
const bodyParser =  require("body-parser");
const session = require("express-session");


var app = express();

const hbs = exphbs.create({
  defaultLayout: 'main',
  layoutsDir: `${__dirname}/views/layouts`,
  partialsDir: `${__dirname}/views/partials`});

app.engine("handlebars", hbs.engine);
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'handlebars');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(compression());
app.use(helmet({contentSecurityPolicy: false}));

app.use(session({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true
}))

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
// app.use(csrf({cookie: true}))

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use("/account/", accounts);
app.use("/api/questions/", questions);
app.use("/api/answers/", answer);
// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
